<?php

/** The name of the database for WordPress */
define( 'DB_NAME', '' );

/** MySQL database username */
define( 'DB_USER', '' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '192.168.0.11' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'CS_ENVIRONMENT', 'dev' );

/* * #@- */
$hostname = isset( $_SERVER[ 'HTTPS' ] ) && strtolower( $_SERVER[ 'HTTPS' ] ) == 'on' ? 'https' : 'http';
$hostname .= '://' . $_SERVER[ 'HTTP_HOST' ] . '/';

define( 'WP_HOME', $hostname . 'code-stud.io/codestudio/src/' );
define( 'WP_SITEURL', $hostname . 'code-stud.io/codestudio/src/' );
define( 'FS_METHOD', 'direct' );

//ini_set( 'display_errors', 0 );

