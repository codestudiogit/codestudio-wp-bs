<?php

return array(
	/** Loggers attached to every command */
	"_" => function () {
		return array(
			new \Monolog\Handler\StreamHandler( 'error.log', \Monolog\Logger::ERROR ),
		);
	}
);
