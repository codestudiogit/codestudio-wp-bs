<?php

/**
 *
 * 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
$devWhitelist = array( "127.0.0.1", "::1" );

if ( in_array( $_SERVER[ 'SERVER_ADDR' ], $devWhitelist ) || strrpos( $_SERVER[ 'SERVER_ADDR' ], '192.1' ) === 0 ) {
	$config_file = 'config/wp-config.dev.php';
} else {
	$config_file = 'config/wp-config.prod.php';
}

$path = dirname( __FILE__ ) . '/';
if ( file_exists( $path . $config_file ) ) {
	// include the config file if it exists, otherwise WP is going to fail
	require_once $path . $config_file;
}

/* * #@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'GJcd!&tfwfC6Hnk=mx4gUU(7Ocm4KDz2?+.GfOlu*#2p.}NV)k>63FkDQ&BFMpnd' );
define( 'SECURE_AUTH_KEY', '>OPZb<=F/jMlFxJs:&^Ee~?):Imwb#|D-#3+-,<R rgwoc,uvj<@(ExpWKc]tbbF' );
define( 'LOGGED_IN_KEY', '|7hVGa)VLoWXJp M|=:ML1tYzEA!VM4VAR:bSTb$>yzP?GPHCBj.%}vwh|]-gAL|' );
define( 'NONCE_KEY', 'zi5[;7^^g-0SzUE8xw9jX||l%$o}.e.x-#l`2P{)zXGwzL6+VDyM0Rj?Q)T_ni(:' );
define( 'AUTH_SALT', 'y&elSz5PL`#kl)SB6Tf-c@Mq^n?0Tj:8oa/1b|*..-Ggz~TFM@H7!<iDXg1Ikmw,' );
define( 'SECURE_AUTH_SALT', 'wh#j{!XX_}-,0(0ae]5|h4{L1j&@mS|Bz&<VcTHd.??)~VZF|>wSCAOMT%v6nAo3' );
define( 'LOGGED_IN_SALT', 'k-v2GU-K+ue,|Fc(*!*+6KP]A8iXsa|Ws>4paFSbU d?PL~d.jo`p`1Ai(9h[n|_' );
define( 'NONCE_SALT', 'G}NtRNZMkfC[D!:!e}!_Z[-&{ILl5wZ0H<j/_p6I(4<472|D+jZBC`x*5%Ar3h(o' );

/* * #@- */

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
